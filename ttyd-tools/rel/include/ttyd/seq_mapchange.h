#pragma once

#include <cstdint>

namespace ttyd::seq_mapchange {

  extern "C" {

  // _relUnLoad
  void _load(const char *currentMap, const char *nextMap, const char *nextBero);
  void _unload(const char *currentMap, const char *nextMap, const char *nextBero);
  void seq_mapChangeMain(uint32_t unk_0, uint32_t unk_1, uint32_t unk_2, uint32_t unk_3, uint32_t unk_4, uint32_t unk_5);
  // seq_mapChangeExit
  // seq_mapChangeInit

  extern char NextBero[];
  extern char NextMap[];
  extern char NextArea[];
  extern char rel_bss[];
  }

}  // namespace ttyd::seq_mapchange