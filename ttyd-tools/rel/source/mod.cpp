#include "mod.h"

#include "patch.h"

#include <gc/os.h>
#include <gc/OSLink.h>
#include <gc/mtx.h>
#include <gc/types.h>
#include <ttyd/battle_database_common.h>
#include <ttyd/battle.h>
#include <ttyd/battle_ac.h>
#include <ttyd/battle_actrecord.h>
#include <ttyd/battle_damage.h>
#include <ttyd/battle_enemy_item.h>
#include <ttyd/battle_event_cmd.h>
#include <ttyd/battle_event_default.h>
#include <ttyd/battle_item_data.h>
#include <ttyd/battle_mario.h>
#include <ttyd/battle_menu_disp.h>
#include <ttyd/battle_seq.h>
#include <ttyd/battle_stage_object.h>
#include <ttyd/battle_sub.h>
#include <ttyd/battle_unit.h>
#include <ttyd/battle_weapon_power.h>
#include <ttyd/dispdrv.h>
#include <ttyd/fontmgr.h>
#include <ttyd/filemgr.h>
#include <ttyd/item_data.h>
#include <ttyd/mario.h>
#include <ttyd/mario_party.h>
#include <ttyd/mario_pouch.h>
#include <ttyd/mariost.h>
#include <ttyd/memory.h>
#include <ttyd/npcdrv.h>
#include <ttyd/system.h>
#include <ttyd/evt_sub.h>
#include <ttyd/evtmgr.h>
#include <ttyd/evtmgr_cmd.h>
#include <evt_cmd.h>

#include <ttyd/unit_party_christine.h>
#include <ttyd/unit_party_chuchurina.h>
#include <ttyd/unit_party_clauda.h>
#include <ttyd/unit_party_nokotarou.h>
#include <ttyd/unit_party_sanders.h>
#include <ttyd/unit_party_vivian.h>
#include <ttyd/unit_party_yoshi.h>

#include <cinttypes>
#include <cstdio>
#include <cstring>
#include <common_types.h>

namespace evt = ::ttyd::evtmgr_cmd;
::gc::OSLink::OSModuleInfo *g_CurrentAdditionalRelModule = nullptr;

// #region Specific Scripts
EVT_DEFINE_USER_FUNC(ToggleCustomAcGaugeColors) {
  bool on = ttyd::evtmgr_cmd::evtGetValue(evt, evt->evtArguments[0]) > 0;

  // (sections being indexed from 0)
  // section 1 - off
  *(uint8_t *)0x800f93eb = on ? 0x00 : 0x1d;  // R
  *(uint8_t *)0x800f93f3 = on ? 0x00 : 0x23;  // G
  *(uint8_t *)0x800f93fb = on ? 0x7F : 0xa3;  // B

  // section 1 - on
  *(uint8_t *)0x800f9bf7 = on ? 0x00 : 0x2e;  // R
  *(uint8_t *)0x800f9bff = on ? 0x00 : 0xb4;  // G
  *(uint8_t *)0x800f9c07 = on ? 0xFF : 0xf2;  // B

  // section 2 - off
  *(uint8_t *)0x800f94bb = on ? 0x00 : 0x46;  // R
  *(uint8_t *)0x800f94c3 = on ? 0x7F : 0x0c;  // G
  *(uint8_t *)0x800f94cb = on ? 0x00 : 0xb4;  // B

  // section 2 - on
  *(uint8_t *)0x800f9d37 = on ? 0x00 : 0x75;  // R
  *(uint8_t *)0x800f9d3f = on ? 0xFF : 0x70;  // G
  *(uint8_t *)0x800f9d47 = on ? 0x00 : 0xff;  // B

  // section 3 - off
  *(uint8_t *)0x800f958f = on ? 0x7F : 0x73;  // R
  *(uint8_t *)0x800f9597 = on ? 0x00 : 0x0d;  // G
  *(uint8_t *)0x800f959f = on ? 0x00 : 0x13;  // B

  // section 3 - on
  *(uint8_t *)0x800f9e4b = on ? 0xFF : 0xf3;  // R
  *(uint8_t *)0x800f9e53 = on ? 0x00 : 0x04;  // G
  *(uint8_t *)0x800f9e5b = on ? 0x00 : 0xbc;  // B

  // joiner section 1-2
  *(uint8_t *)0x800f978f = on ? 0xFF : 0x7d;  // R
  *(uint8_t *)0x800f9797 = on ? 0x00 : 0x2c;  // G
  *(uint8_t *)0x800f979f = on ? 0x00 : 0xb5;  // B

  // joiner section 2-3
  *(uint8_t *)0x800f9847 = on ? 0x00 : 0xa1;  // R
  *(uint8_t *)0x800f984f = on ? 0x00 : 0x1b;  // G
  *(uint8_t *)0x800f9857 = on ? 0xFF : 0x55;  // B

  return 2;
}

EVT_BEGIN(ClaudaBreathAttackHook)
UNCHECKED_USER_FUNC(ToggleCustomAcGaugeColors, 1)
RUN_CHILD_EVT(ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.attack_evt_code)
UNCHECKED_USER_FUNC(ToggleCustomAcGaugeColors, 0)
RETURN()
EVT_END()

EVT_DEFINE_USER_FUNC(evt_getcurmapptr) {
  gc::OSLink::OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pMapAlloc;
  evt::evtSetValue(evt, evt->evtArguments[0], reinterpret_cast<uintptr_t>(cur_module));
  return 2;
}

EVT_DEFINE_USER_FUNC(evt_getaddmapptr) {
  evt::evtSetValue(evt, evt->evtArguments[0], reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule));
  return 2;
}
// #endregion

EVT_BEGIN(tou2_unit_chorobon_in_gon_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x3890)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x227c)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

EVT_BEGIN(hei_unit_chorobon_in_hei_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getcurmapptr, LW(15))
	ADD(LW(15), (int32_t)0x0001b108)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getcurmapptr, LW(15))
	ADD(LW(15), (int32_t)0x000187e8)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

EVT_BEGIN(tou2_unit_chorobon_in_hei_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getcurmapptr, LW(15))
	ADD(LW(15), (int32_t)0x0001b108)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x51c)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

EVT_BEGIN(tik_unit_kuriboo_in_tik_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getcurmapptr, LW(15))
	ADD(LW(15), (int32_t)0x0002e23c)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getcurmapptr, LW(15))
	ADD(LW(15), (int32_t)0x0002ab58)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

EVT_BEGIN(tou2_unit_chorobon_in_tou2_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x20cc)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getcurmapptr, LW(15))
	ADD(LW(15), (int32_t)0x0002ed08)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

EVT_BEGIN(hei_unit_chorobon_in_win_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x3a50)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x243c)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

EVT_BEGIN(tou2_unit_chorobon_in_win_AttackEvent)
	USER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))
	IF_EQUAL(LW(0), 0)
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x3a50)
	ELSE()
	UNCHECKED_USER_FUNC(evt_getaddmapptr, LW(15))
	ADD(LW(15), (int32_t)0x5694)
	END_IF()
	RUN_CHILD_EVT(LW(15))
	RETURN()
	EVT_END()

namespace mod {

  namespace {
    using ::gc::OSLink::OSModuleInfo;
    using ::mod::infinite_pit::Mod;
    using ::ttyd::battle::BattleWork;
    using ::ttyd::battle::BattleWorkCommandCursor;
    using ::ttyd::battle::BattleWorkCommandOperation;
    using ::ttyd::battle::BattleWorkCommandWeapon;
    using ::ttyd::battle_database_common::BattleGroupSetup;
    using ::ttyd::battle_database_common::BattleUnitKind;
    using ::ttyd::battle_database_common::BattleUnitKindPart;
    using ::ttyd::battle_database_common::BattleUnitSetup;
    using ::ttyd::battle_database_common::BattleWeapon;
    using ::ttyd::battle_database_common::PointDropData;
    using ::ttyd::battle_unit::BattleWorkUnit;
    using ::ttyd::battle_unit::BattleWorkUnitPart;
    using ::ttyd::evtmgr::EvtEntry;
    using ::ttyd::item_data::ItemData;
    using ::ttyd::item_data::itemDataTable;
    using ::ttyd::mario_pouch::PouchData;
    using ::ttyd::mariost::g_MarioSt;
    using ::ttyd::npcdrv::FbatBattleInformation;
    using ::ttyd::npcdrv::NpcBattleInfo;
    using ::ttyd::npcdrv::NpcEntry;
    using ::ttyd::system::irand;

    namespace BattleUnitType = ::ttyd::battle_database_common::BattleUnitType;
    namespace ItemType = ::ttyd::item_data::ItemType;
    namespace ItemUseLocation = ::ttyd::item_data::ItemUseLocation_Flags;

    using PFN_DVDMgrOpen = void *(*)(const char *, uint32_t, uint16_t);       // path, unk, unk
    using PFN_DVDMgrRead = uint32_t (*)(void *, void *, uint32_t, uint32_t);  // file, dest, len, offset
    using PFN_DVDMgrClose = bool (*)(void *);                                 // file
    PFN_DVDMgrOpen DVDMgrOpen = (PFN_DVDMgrOpen)0x80144e88;
    PFN_DVDMgrRead DVDMgrRead = (PFN_DVDMgrRead)0x80144e18;
    PFN_DVDMgrClose DVDMgrClose = (PFN_DVDMgrClose)0x80144dd4;

    void (*marioStMain_trampoline_)() = nullptr;
    BattleWorkUnit *(*g_BtlUnit_Entry_trampoline)(BattleUnitSetup *) = nullptr;
    int32_t (*g_BattleCalculateDamage_trampoline)(BattleWorkUnit *, BattleWorkUnit *, BattleWorkUnitPart *, BattleWeapon *, uint32_t *, uint32_t) = nullptr;
    int32_t (*g_BattleCalculateFpDamage_trampoline)(BattleWorkUnit *, BattleWorkUnit *, BattleWorkUnitPart *, BattleWeapon *, uint32_t *, uint32_t) = nullptr;
    bool (*g_OSLink_trampoline)(OSModuleInfo *, void *) = nullptr;
    void (*g_pouchRevisePartyParam_trampoline)() = nullptr;
    void (*g_unk_US_EU_08_800d1364_trampoline)(char *, char *) = nullptr;
    void (*g_filemgr_fileAlloc_trampoline)(char *, void *) = nullptr;
    uint32_t (*g_make_breath_weapon_trampoline)(ttyd::evtmgr::EvtEntry *) = nullptr;
    void (*g_seq_loadExit_trampoline)() = nullptr;

    const char *g_CurrentAdditionalRelArea = nullptr;
    alignas(0x10) char g_AdditionalRelBss[0x3d4];

    uint8_t allocatedBtlGrpMax[100];
    BattleUnitSetup *allocatedBtlgrp[100];
    uint16_t allocatedIndex = 0;

    Mod *gMod = nullptr;
  }  // namespace

  void FixBattleOffsetsInRel();
  void PreBattleCalculateDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module);
  void PreBattleCalculateFpDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module);
  void PostBattleCalculateDamage(uint32_t moduleId, BattleWorkUnit *attacker, int32_t &damage);
  void PreBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind);
  void PostBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind, BattleWorkUnit *work_unit);
  void ChangeItems(uint32_t moduleId);

  void main() {
    Mod *mod = new Mod();
    mod->Init();
  }

  Mod::Mod() {
  }

  BattleUnitSetup *InitEnemyData(int enemyCount, BattleGroupSetup *group_setup) {
    bool enoughEnemySpace = group_setup->num_enemies >= enemyCount;
    group_setup->num_enemies = enemyCount;
    BattleUnitSetup *enemy_data = group_setup->enemy_data;

    // the current array is large enough for our edits
    if (enoughEnemySpace) {
      return enemy_data;
    }

    int foundIndex = -1;
    for (uint16_t i = 0; i < allocatedIndex; i++) {
      if (allocatedBtlgrp[i] == enemy_data) {
        foundIndex = i;
        break;
      }
    }

    // we've already allocated an array for this setup
    if (foundIndex >= 0) {
      if (allocatedBtlGrpMax[foundIndex] >= enemyCount) {
        // it's large enough, return the already set pointer
        return enemy_data;
      } else {
        // it's too small, free it
        ttyd::memory::__memFree(0, allocatedBtlgrp[foundIndex]);
      }
    }

    BattleUnitSetup *enemies_ptr = (BattleUnitSetup *)ttyd::memory::__memAlloc(0, group_setup->num_enemies * sizeof(BattleUnitSetup));

    int grpIndex = foundIndex >= 0 ? foundIndex : allocatedIndex++;
    allocatedBtlgrp[grpIndex] = enemies_ptr;
    allocatedBtlGrpMax[grpIndex] = enemyCount;

    for (int i = 0; i < group_setup->num_enemies; ++i) {
      BattleUnitSetup *new_enemy_ptr = reinterpret_cast<BattleUnitSetup *>(enemies_ptr + i);
      memcpy(new_enemy_ptr, group_setup->enemy_data, sizeof(BattleUnitSetup));
    }

    return enemies_ptr;
  }

  void UnloadAdditionalRel() {
    if (g_CurrentAdditionalRelArea) {
      gc::OSLink::OSUnlink(g_CurrentAdditionalRelModule);
      ttyd::memory::__memFree(0, g_CurrentAdditionalRelModule);
      g_CurrentAdditionalRelArea = nullptr;
      g_CurrentAdditionalRelModule = nullptr;
    }
  }

  void LoadAdditionalRel(char *additionalRel) {
    if (g_CurrentAdditionalRelArea && strcmp(g_CurrentAdditionalRelArea, additionalRel) != 0) {
      UnloadAdditionalRel();
    }

    if (!g_CurrentAdditionalRelArea) {
      g_CurrentAdditionalRelArea = additionalRel;

      char pathBuffer[48];
      sprintf(pathBuffer, "/rel/%s.rel", g_CurrentAdditionalRelArea);

      void *entry = DVDMgrOpen(pathBuffer, 2, 0);
      uint32_t length = *(uint32_t *)((uint32_t)entry + 0x74);
      length = (length + 0x1fU) & 0xffffffe0;  // round up 32B

      g_CurrentAdditionalRelModule = (OSModuleInfo *)ttyd::memory::__memAlloc(0, length);
      DVDMgrRead(entry, g_CurrentAdditionalRelModule, length, 0);
      DVDMgrClose(entry);

      g_OSLink_trampoline(g_CurrentAdditionalRelModule, g_AdditionalRelBss);
    }

    FixBattleOffsetsInRel();
  }

  bool IsPtrInCurrentModule(uint32_t ptr) {
    bool in_cur_module = true;
    if (g_CurrentAdditionalRelModule != nullptr) {
      const OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pRelFileBase;
      const uintptr_t module_ptr = reinterpret_cast<uintptr_t>(cur_module);

      const uint32_t additional_module_ptr = reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule);
      if (module_ptr < additional_module_ptr) {
        in_cur_module = ptr < additional_module_ptr;
      } else {
        in_cur_module = ptr > module_ptr;
      }
    }

    return in_cur_module;
  }

  // #region Specific Methods
char *AlternativeSkins(char *fn) {
  if ((uint32_t)fn < 0x80000000 || (uint32_t)fn >= 0x81800000) {  // GC memory range
    return fn;
  }

  switch (ttyd::mariost::g_MarioSt->pMapAlloc->id) {
    case ModuleId::MRI: {
      if (strcmp(fn, "a/c_kuribo-") == 0) {
        fn = (char *)"a/c_kuribo_p-";
      }
    } break;
    case ModuleId::WIN: {
      if (strcmp(fn, "a/c_kuribo-") == 0) {
        fn = (char *)"a/c_kuribo_p-";
      }
    } break;
    default:
      break;
  }

  return fn;
}
// #endregion


  void Mod::Init() {
    // #region Specific Init
static constexpr uint32_t fileAllocfInstructionChange = 0x4bffff39;
patch::writePatch((void *)0x8018a6c0, &fileAllocfInstructionChange, sizeof(uint32_t));

g_filemgr_fileAlloc_trampoline = patch::hookFunction((void (*)(char *, void *))0x8018a5f8, [](char *fn, void *unk) {
  return g_filemgr_fileAlloc_trampoline(AlternativeSkins(fn), unk);
});

ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.base_fp_cost = 4;
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.element = 2;         // Ice type
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.damage_pattern = 0;  // remove "blow away"
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.damage_function = (void *)ttyd::battle_weapon_power::weaponGetPowerDefault;
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.gale_force_chance = 0;
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.special_property_flags |= 0x40;    // PiercesDefense
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.special_property_flags &= ~0x200;  // remove the "gale force-only" attribute

static constexpr uint32_t galeForceNewBarPercentages[3] = {0, 40, 88};
memcpy((void *)0x8037a170, &galeForceNewBarPercentages, sizeof(uint32_t) * 3);  // 0x8037a15c + 0x14, in partyClaudaAttack_BreathAttack

static constexpr uint32_t galeForceDamageArray[3 * 3] = {0, 1, 2, 1, 2, 3, 1, 3, 4};
g_make_breath_weapon_trampoline = patch::hookFunction(
    ttyd::unit_party_clauda::_make_breath_weapon,
    [](ttyd::evtmgr::EvtEntry *entry) {
      g_make_breath_weapon_trampoline(entry);

      int32_t unitIdx = ttyd::battle_sub::BattleTransID(entry, -2);
      BattleWorkUnit *unitPtr = ttyd::battle::BattleGetUnitPtr(ttyd::battle::g_BattleWork, unitIdx);

      // infinite-pit doesn't have a struct field for extra_work, and I'm too lazy to add it
      auto weapon = *(ttyd::battle_database_common::BattleWeapon **)((uintptr_t)unitPtr + 0x314);

      uint32_t barFilledPercentage = weapon->gale_force_chance;
      weapon->gale_force_chance = 0;

      int16_t level = ttyd::mario_pouch::pouchGetPtr()->party_data[5].attack_level;

      uint32_t barSection = 0;
      if (barFilledPercentage >= galeForceNewBarPercentages[2]) {
        barSection = 2;
      } else if (barFilledPercentage >= galeForceNewBarPercentages[1]) {
        barSection = 1;
      }

      weapon->damage_function_params[0] = galeForceDamageArray[barSection + level * 3];
      if (barSection > 0) {
        weapon->freeze_chance = 20;
        weapon->freeze_time = 1;
      }  // this weapon is a copy, so its data does not carry over and the freeze stats do not need to be reset

      return (uint32_t)2;
    });

// switch JP names for frankie & don pianta's tribe descriptions
*(uintptr_t *)0x80332e70 = 0x802e5170;  // マフィアボス -> ピートン
*(uintptr_t *)0x80332ec4 = 0x802e5154;  // the opposite
// #endregion


    gMod = this;

    g_unk_US_EU_08_800d1364_trampoline = patch::hookFunction(
        (void (*)(char *, char *))0x800d1364,
        [](char *unk_1, char *unk_2) {
          uint32_t orig = ttyd::mariost::g_MarioSt->language;
          ttyd::mariost::g_MarioSt->language = 0x3;  // French

          g_unk_US_EU_08_800d1364_trampoline(unk_1, unk_2);

          ttyd::mariost::g_MarioSt->language = orig;
        });

    // Hook the game's main function, so updateEarly runs exactly once per frame
    marioStMain_trampoline_ =
        patch::hookFunction(ttyd::mariost::marioStMain, []() {
          gMod->Update();
        });

    g_BattleCalculateDamage_trampoline = patch::hookFunction(
        ttyd::battle_damage::BattleCalculateDamage, [](BattleWorkUnit *attacker, BattleWorkUnit *target, BattleWorkUnitPart *target_part, BattleWeapon *weapon, uint32_t *unk0, uint32_t unk1) {
          int32_t base_atk = weapon->damage_function_params[0];
          bool in_cur_module = IsPtrInCurrentModule(reinterpret_cast<uintptr_t>(attacker->unit_kind_params));

          // 0x100000 = recoil attack
          bool should_affect_damage = attacker->current_kind <= BattleUnitType::BONETAIL && !(weapon->target_property_flags & 0x100000) && !weapon->item_id;

          if (should_affect_damage) {
            PreBattleCalculateDamage(attacker, weapon, base_atk, in_cur_module);
          }

          int32_t damage = g_BattleCalculateDamage_trampoline(attacker, target, target_part, weapon, unk0, unk1);
          weapon->damage_function_params[0] = base_atk;

          if (should_affect_damage) {
            PostBattleCalculateDamage(in_cur_module ? ttyd::mariost::g_MarioSt->pRelFileBase->id : g_CurrentAdditionalRelModule->id, attacker, damage);

            if (damage > 99) {
              damage = 99;
            }
          }

          return damage;
        });

    g_BattleCalculateFpDamage_trampoline = patch::hookFunction(
        ttyd::battle_damage::BattleCalculateFpDamage, [](BattleWorkUnit *attacker, BattleWorkUnit *target, BattleWorkUnitPart *target_part, BattleWeapon *weapon, uint32_t *unk0, uint32_t unk1) {
          int32_t base_atk = weapon->fp_damage_function_params[0];
          bool in_cur_module = IsPtrInCurrentModule(reinterpret_cast<uintptr_t>(attacker->unit_kind_params));
          bool should_affect_damage = attacker->current_kind <= BattleUnitType::BONETAIL && !weapon->item_id;

          if (should_affect_damage) {
            PreBattleCalculateFpDamage(attacker, weapon, base_atk, in_cur_module);
          }

          int32_t damage = g_BattleCalculateFpDamage_trampoline(attacker, target, target_part, weapon, unk0, unk1);
          weapon->fp_damage_function_params[0] = base_atk;

          return damage;
        });

    g_BtlUnit_Entry_trampoline = patch::hookFunction(
        ttyd::battle_unit::BtlUnit_Entry, [](BattleUnitSetup *unit_setup) {
          BattleUnitKind *unit_kind = unit_setup->unit_kind_params;
          bool in_cur_module = IsPtrInCurrentModule(reinterpret_cast<uintptr_t>(unit_kind));

          OSModuleInfo *cur_module = in_cur_module ? ttyd::mariost::g_MarioSt->pRelFileBase : g_CurrentAdditionalRelModule;

          bool is_enemy = unit_kind->unit_type <= BattleUnitType::BONETAIL;
          if (is_enemy) {
            // Used as sentinel to see if stats have already been changed for this type.
            if (!(unit_kind->run_rate & 1)) {
              PreBattleUnitEntry(cur_module->id, unit_kind);
            }

            // Set sentinel bit so enemy's stats aren't changed multiple times.
            unit_kind->run_rate |= 1;
          }

          BattleWorkUnit *work_unit = g_BtlUnit_Entry_trampoline(unit_setup);
          if (is_enemy) {
            PostBattleUnitEntry(cur_module->id, unit_kind, work_unit);
          }

          return work_unit;
        });

    // Partners Hp
    static int16_t custom_party_max_hp_table[32] = {
0, 0, 0, 0, 
10, 20, 30, 200, 
10, 15, 25, 200, 
15, 25, 35, 200, 
10, 20, 30, 200, 
15, 20, 30, 200, 
20, 30, 40, 200, 
15, 20, 25, 200, 
};
    patch::writePatch(&ttyd::mario_pouch::_party_max_hp_table, &custom_party_max_hp_table, sizeof(custom_party_max_hp_table));

    // Partners stats
    g_pouchRevisePartyParam_trampoline = patch::hookFunction(reinterpret_cast<void (*)()>(0x800d3430), []() {
      [[maybe_unused]] uintptr_t *done = nullptr;
      [[maybe_unused]] const BattleUnitKind goombella = ttyd::unit_party_christine::unitdata_Party_Christine;
if (ttyd::mario_pouch::pouchGetPtr()->party_data[1].attack_level == 0) {
ttyd::unit_party_christine::partyWeapon_ChristineNormalAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_christine::partyWeapon_ChristineMonosiri.target_class_flags |= 0x2000000;
}
if (ttyd::mario_pouch::pouchGetPtr()->party_data[1].attack_level == 1) {
ttyd::unit_party_christine::partyWeapon_ChristineNormalAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_christine::partyWeapon_ChristineMonosiri.target_class_flags |= 0x2000000;
ttyd::unit_party_christine::partyWeapon_ChristineRenzokuAttack.target_class_flags |= 0x2000000;
}
if (ttyd::mario_pouch::pouchGetPtr()->party_data[1].attack_level == 2) {
ttyd::unit_party_christine::partyWeapon_ChristineNormalAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_christine::partyWeapon_ChristineMonosiri.target_class_flags |= 0x2000000;
ttyd::unit_party_christine::partyWeapon_ChristineRenzokuAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_christine::partyWeapon_ChristineKiss.target_class_flags |= 0x2000000;
}
[[maybe_unused]] const BattleUnitKind koops = ttyd::unit_party_nokotarou::unitdata_Party_Nokotarou;
if (ttyd::mario_pouch::pouchGetPtr()->party_data[2].attack_level == 0) {
ttyd::unit_party_nokotarou::partyWeapon_NokotarouFirstAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouKouraAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouSyubibinKoura.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouSyubibinKoura.target_class_flags &= ~0x1000000;
}
if (ttyd::mario_pouch::pouchGetPtr()->party_data[2].attack_level == 1) {
ttyd::unit_party_nokotarou::partyWeapon_NokotarouFirstAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouKouraAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouSyubibinKoura.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouSyubibinKoura.target_class_flags &= ~0x1000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouKouraGuard.target_class_flags |= 0x2000000;
}
if (ttyd::mario_pouch::pouchGetPtr()->party_data[2].attack_level == 2) {
ttyd::unit_party_nokotarou::partyWeapon_NokotarouFirstAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouKouraAttack.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouSyubibinKoura.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouSyubibinKoura.target_class_flags &= ~0x1000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouKouraGuard.target_class_flags |= 0x2000000;
ttyd::unit_party_nokotarou::partyWeapon_NokotarouTsuranukiKoura.target_class_flags |= 0x2000000;
}
[[maybe_unused]] const BattleUnitKind flurrie = ttyd::unit_party_clauda::unitdata_Party_Clauda;
[[maybe_unused]] const BattleUnitKind yoshi = ttyd::unit_party_yoshi::unitdata_Party_Yoshi;
[[maybe_unused]] const BattleUnitKind vivian = ttyd::unit_party_vivian::unitdata_Party_Vivian;
[[maybe_unused]] const BattleUnitKind bobbery = ttyd::unit_party_sanders::unitdata_Party_Sanders;
[[maybe_unused]] const BattleUnitKind mowz = ttyd::unit_party_chuchurina::unitdata_Party_Chuchurina;

      g_pouchRevisePartyParam_trampoline();
    });

    g_seq_loadExit_trampoline = patch::hookFunction(reinterpret_cast<void (*)()>(0x800f727c), []() {  // seq_loadExit
      reinterpret_cast<void (*)()>(0x800d3430)();                                                     // pouchRevisePartyParam
      g_seq_loadExit_trampoline();
    });

    // Menu Colors
    *(uint32_t *)0x804231b8 = 0x12a149FF; // MARIO - border
*(uint32_t *)0x804231bc = 0x60d336FF; // MARIO - background
*(uint32_t *)0x804231c0 = 0xc7e3bfFF; // MARIO - icon
*(uint32_t *)0x804231c4 = 0xa26ac8FF; // PARTY - border
*(uint32_t *)0x804231c8 = 0xcda9daFF; // PARTY - background
*(uint32_t *)0x804231cc = 0xe4d0ecFF; // PARTY - icon
*(uint32_t *)0x804231dc = 0xa7752fFF; // BADGES - border
*(uint32_t *)0x804231e0 = 0xd3a46fFF; // BADGES - background
*(uint32_t *)0x804231e4 = 0xfedecdFF; // BADGES - icon

    g_OSLink_trampoline = patch::hookFunction(
        gc::OSLink::OSLink, [](OSModuleInfo *new_module, void *bss) {
          bool result = g_OSLink_trampoline(new_module, bss);
          if (new_module != nullptr && result) {
            for (uint16_t i = 0; i < allocatedIndex; i++) {
              ttyd::memory::__memFree(0, allocatedBtlgrp[i]);
            }
            allocatedIndex = 0;

            [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(new_module);
            switch (new_module->id) {
                case ModuleId::TOU2 : {
LoadAdditionalRel((char *)"additional-rel-for-tou2");
} break;
case ModuleId::WIN : {
LoadAdditionalRel((char *)"additional-rel-for-win");
} break;
case ModuleId::GON : {
LoadAdditionalRel((char *)"additional-rel-for-gon");
} break;
case ModuleId::HEI : {
LoadAdditionalRel((char *)"additional-rel-for-hei");
} break;
              default:
                UnloadAdditionalRel();
                FixBattleOffsetsInRel();
                break;
            }
            ChangeItems(new_module->id);

            // #region Specific OSLink
if (new_module->id == ModuleId::WIN) {
  /// Flurrie entry: set fade color
  *(uint32_t *)0x805C79F8 = 0xA9;  // R
  *(uint32_t *)0x805C7A04 = 0xDC;  // G
  *(uint32_t *)0x805C7A10 = 0xFF;  // B

  /// Flurrie entry: replace rose texture
  void *tex_addr = (void *)(0x805ba9a0 + 0xe180);
  void *entry = DVDMgrOpen("/mod/snowflake.tex", 2, 0);
  uint32_t length = *(uint32_t *)((uintptr_t)entry + 0x74);
  DVDMgrRead(entry, tex_addr, (length + 0x1fU) & 0xffffffe0, 0);
  DVDMgrClose(entry);
} else if (new_module->id == ModuleId::TOU) {
  static constexpr uintptr_t reversableEvtMsgPrintOffsets[] = {0x805CDDAC, 0x805CDDD0, 0x805CDDF8, 0x805CDE20, 0x805CDEF8, 0x805CDF4C, 0x805CE028, 0x805CE04C};

  // reverse the order of LW(3) and LW(1) in all relevant calls to evt_msg_print_insert in evt_tou_match_make_default_sub
  // (i.e. reverse the order of %d and %s)
  for (uint8_t i = 0; i < 8; i++) {
    uintptr_t offset = reversableEvtMsgPrintOffsets[i];
    *(uint32_t *)(offset + 0x18) -= 2;
    *(uint32_t *)(offset + 0x1c) += 2;
  }
} else if (new_module->id == ModuleId::GOR) {
  // change items needed for access to Pianteone
  *(uint32_t *)0x806124b0 = ItemType::VOLT_SHROOM;   // DRIED_SHROOM to VOLT_SHROOM
  *(uint32_t *)0x806124c4 = ItemType::THUNDER_BOLT;  // DIZZY_DIAL to THUNDER_BOLT
} else if (new_module->id == ModuleId::GON) {
  // swap FX A & Storage Key
  *(uint32_t *)0x805C4AF8 = ItemType::ATTACK_FX_R;      // in gon_03_init_evt
  *(uint32_t *)0x805CA6FC = ItemType::CASTLE_KEY_000C;  // in gon_06_init_evt
}
// #endregion

if (new_module->id == ModuleId::GON) {
*(void **)(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x21e8 + 0x1c) = (void *)&tou2_unit_chorobon_in_gon_AttackEvent;
}
if (new_module->id == ModuleId::HEI) {
*(void **)(module_ptr + 0x00018c38 + 0x1c) = (void *)&hei_unit_chorobon_in_hei_AttackEvent;
*(void **)(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x488 + 0x1c) = (void *)&tou2_unit_chorobon_in_hei_AttackEvent;
}
if (new_module->id == ModuleId::TIK) {
*(void **)(module_ptr + 0x0002aa80 + 0x1c) = (void *)&tik_unit_kuriboo_in_tik_AttackEvent;
}
if (new_module->id == ModuleId::TOU2) {
*(void **)(module_ptr + 0x0002f158 + 0x1c) = (void *)&tou2_unit_chorobon_in_tou2_AttackEvent;
}
if (new_module->id == ModuleId::WIN) {
*(void **)(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x23a8 + 0x1c) = (void *)&hei_unit_chorobon_in_win_AttackEvent;
*(void **)(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x5600 + 0x1c) = (void *)&tou2_unit_chorobon_in_win_AttackEvent;
}
          }

          return result;
        });

    // Initialize typesetting early (to display mod information on title screen)
    ttyd::fontmgr::fontmgrTexSetup();
    patch::hookFunction(ttyd::fontmgr::fontmgrTexSetup, []() {});
  }

  void Mod::Update() {
    // Call original function.
    marioStMain_trampoline_();
  }

  void FixBattleOffsetsInRel() {
    OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pMapAlloc;
    [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(cur_module);

    [[maybe_unused]] BattleGroupSetup *group_setup = nullptr;
    [[maybe_unused]] int dice = -1;
    switch (cur_module->id) {
        case ModuleId::GON: {
dice = ttyd::system::irand(100);
uint btlgrp_gon_gon_02_02_off_1Loadouts[] = {0x0001656c};
for (const uint &loadout : btlgrp_gon_gon_02_02_off_1Loadouts) {
group_setup = reinterpret_cast<BattleGroupSetup *>(module_ptr + loadout);
 if (0 < dice && dice <= 100) {
group_setup->enemy_data = InitEnemyData(1, group_setup);
group_setup->enemy_data[0].unit_kind_params = reinterpret_cast<BattleUnitKind *>(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x2020);
group_setup->enemy_data[0].position = {90, 0, 0};
}
}
} break;
case ModuleId::HEI: {
dice = ttyd::system::irand(100);
uint btlgrp_hei_hei_13_02_off_1Loadouts[] = {0x000173cc, 0x0001747c, 0x0001755c, 0x0001766c};
for (const uint &loadout : btlgrp_hei_hei_13_02_off_1Loadouts) {
group_setup = reinterpret_cast<BattleGroupSetup *>(module_ptr + loadout);
 if (0 < dice && dice <= 100) {
group_setup->enemy_data = InitEnemyData(2, group_setup);
group_setup->enemy_data[0].unit_kind_params = reinterpret_cast<BattleUnitKind *>(module_ptr + 0x000185f0);
group_setup->enemy_data[0].position = {70, 0, 0};
group_setup->enemy_data[1].unit_kind_params = reinterpret_cast<BattleUnitKind *>(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x2c0);
group_setup->enemy_data[1].position = {110, 0, 10};
}
}
} break;
case ModuleId::TIK: {
dice = ttyd::system::irand(100);
uint btlgrp_tik_tik_01_02_off_1Loadouts[] = {0x000264e0};
for (const uint &loadout : btlgrp_tik_tik_01_02_off_1Loadouts) {
group_setup = reinterpret_cast<BattleGroupSetup *>(module_ptr + loadout);
 if (0 < dice && dice <= 100) {
group_setup->enemy_data = InitEnemyData(1, group_setup);
group_setup->enemy_data[0].unit_kind_params = reinterpret_cast<BattleUnitKind *>(module_ptr + 0x0002a7d8);
group_setup->enemy_data[0].position = {90, 0, 0};
}
}
} break;
case ModuleId::WIN: {
dice = ttyd::system::irand(100);
uint btlgrp_win_win_02_02_off_1Loadouts[] = {0x000112d4, 0x00011354, 0x00011404, 0x000114b4};
for (const uint &loadout : btlgrp_win_win_02_02_off_1Loadouts) {
group_setup = reinterpret_cast<BattleGroupSetup *>(module_ptr + loadout);
 if (0 < dice && dice <= 100) {
group_setup->enemy_data = InitEnemyData(2, group_setup);
group_setup->enemy_data[0].unit_kind_params = reinterpret_cast<BattleUnitKind *>(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x21e0);
group_setup->enemy_data[0].position = {70, 0, 0};
group_setup->enemy_data[1].unit_kind_params = reinterpret_cast<BattleUnitKind *>(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + 0x5438);
group_setup->enemy_data[1].position = {110, 0, 10};
}
}
} break;
      default:
        break;
    }
  }

  void PreBattleCalculateDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module) {
    const OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pRelFileBase;
    const OSModuleInfo *moduleInfo = in_cur_module ? cur_module : g_CurrentAdditionalRelModule;

    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)attacker->unit_kind_params - (uintptr_t)g_CurrentAdditionalRelModule;
    [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(moduleInfo);

    switch (moduleInfo->id) {
        case ModuleId::HEI: {
switch (attacker->current_kind) {
case BattleUnitType::FUZZY: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x0001a878)) {
altered_atk = 42;
}
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x00018728)) {
altered_atk = 3;
}
} break;
case BattleUnitType::GOLD_FUZZY: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x0001a878)) {
altered_atk = 17;
weapon->sleep_chance = 10;
weapon->sleep_time = 2;
}
} break;
default:
break;
}
} break;
case ModuleId::TIK: {
switch (attacker->current_kind) {
case BattleUnitType::GOOMBA: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x0002df50)) {
altered_atk = 17;
}
} break;
case BattleUnitType::SPUNIA: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x0003e0a0)) {
altered_atk = 20;
}
} break;
default:
break;
}
} break;
case ModuleId::TOU2: {
switch (attacker->current_kind) {
case BattleUnitType::FUZZY: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x0001a878)) {
altered_atk = 42;
}
} break;
default:
break;
}
} break;
case 34: {
switch (addRelFileOffset) {
case 0x1e40: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x289c)) {
altered_atk = 17;
weapon->sleep_chance = 10;
weapon->sleep_time = 2;
}
} break;
default:
break;
}
} break;
case 35: {
switch (addRelFileOffset) {
case 0x21e0: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x4220)) {
altered_atk = 42;
}
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x2490)) {
altered_atk = 3;
}
} break;
case 0x37c4: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x4220)) {
altered_atk = 17;
weapon->sleep_chance = 10;
weapon->sleep_time = 2;
}
} break;
case 0x5438: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x4220)) {
altered_atk = 42;
}
} break;
default:
break;
}
} break;
case 36: {
switch (addRelFileOffset) {
case 0x2020: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x4060)) {
altered_atk = 42;
}
} break;
case 0x3604: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x4060)) {
altered_atk = 17;
weapon->sleep_chance = 10;
weapon->sleep_time = 2;
}
} break;
default:
break;
}
} break;
case 37: {
switch (addRelFileOffset) {
case 0x2c0: {
if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + 0x0001a878)) {
altered_atk = 42;
}
} break;
default:
break;
}
} break;
      default:
        break;
    }

    if (altered_atk < 0) {
      altered_atk = 0;
    }
    if (altered_atk > 99) {
      altered_atk = 99;
    }

    weapon->damage_function_params[0] = altered_atk;
  }

  void PreBattleCalculateFpDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module) {
    const OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pRelFileBase;
    const OSModuleInfo *moduleInfo = in_cur_module ? cur_module : g_CurrentAdditionalRelModule;

    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)attacker->unit_kind_params - (uintptr_t)g_CurrentAdditionalRelModule;
    [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(moduleInfo);

    switch (moduleInfo->id) {
        
      default:
        break;
    }

    if (altered_atk < 0) {
      altered_atk = 0;
    }
    if (altered_atk > 99) {
      altered_atk = 99;
    }

    if (altered_atk > 0 && weapon->fp_damage_function == nullptr) {
      weapon->fp_damage_function = (void *)ttyd::battle_weapon_power::weaponGetFPPowerDefault;
    }
    weapon->fp_damage_function_params[0] = altered_atk;
  }

  void PostBattleCalculateDamage(uint32_t moduleId, BattleWorkUnit *attacker, int32_t &damage) {
    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)attacker->unit_kind_params - (uintptr_t)g_CurrentAdditionalRelModule;

    switch (moduleId) {
        
      default:
        break;
    }
  }

  void PreBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind) {
    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)unit_kind - (uintptr_t)g_CurrentAdditionalRelModule;

    switch (moduleId) {
        case ModuleId::HEI: {
switch (unit_kind->unit_type) {
case BattleUnitType::FUZZY: {
unit_kind->max_hp = 10;
} break;
default:
break;
}
} break;
case 35: {
switch (addRelFileOffset) {
case 0x21e0: {
unit_kind->max_hp = 10;
} break;
default:
break;
}
} break;
      default:
        break;
    }
  }

  void PostBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind, BattleWorkUnit *work_unit) {
    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)unit_kind - (uintptr_t)g_CurrentAdditionalRelModule;

    switch (moduleId) {
        
      default:
        break;
    }
  }

  void ChangeItems(uint32_t moduleId) {
    switch (moduleId) {
        
      default:
        break;
    }
  }
}  // namespace mod
